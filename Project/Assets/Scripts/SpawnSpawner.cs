﻿using UnityEngine;
using System.Collections;

public class SpawnSpawner : MonoBehaviour
{

    public GameObject gravePrefab;

    public void Spawn()
    {
        Instantiate(gravePrefab, Random.insideUnitSphere * 15 + transform.position, transform.rotation);

        //Instantiate(spawnerPrefab, transform.position, transform.rotation);
    }
}

