﻿using UnityEngine;
using System.Collections;

public class PlayerBehaviour : MonoBehaviour
{

    public delegate void UpdateHealth(int newHealth);
    public static event UpdateHealth OnUpdateHealth;

    public int health = 100;
    public int damage = 100;
    public int score;
    public bool activePowerUp;
    public GameObject Hero;
    public GameObject gameController;
    private Animator gunAnim;

    void start()
    {
        gunAnim = GetComponent<Animator>();

        SendHealthData();
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GetComponent<AudioSource>().Play();
            GetComponent<Animator>().SetBool("isFiring", true);
        }
        if (Input.GetMouseButtonUp(0))
        {
            GetComponent<Animator>().SetBool("isFiring", false);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        SendHealthData();

        if (health <= 0)
        {
            Die();
        }
    }

    void Die()
    {
        Application.LoadLevel("GameOver");
    }

    void SendHealthData()
    {
        if (OnUpdateHealth != null)
        {
            OnUpdateHealth(health);
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("PowerUp"))
        {
            activePowerUp = true;
            other.gameObject.SendMessage("TakeDamage", damage);
            Hero.gameObject.SendMessage("PowerUp", activePowerUp);
        }
    }
}
