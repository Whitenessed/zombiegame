﻿using UnityEngine;
using System.Collections;

public class GameUI : MonoBehaviour {

    private int health = 100;
    public int score;
    private string gameInfo = "";

    

    private Rect boxRect = new Rect(10, 10, 300, 50);

    void OnEnable()
    {
        PlayerBehaviour.OnUpdateHealth += HandleonUpdateHealth;
        AddScore.OnSendScore += HandleonSendScore;
    }

    void OnDisable()
    {
        PlayerBehaviour.OnUpdateHealth -= HandleonUpdateHealth;
        AddScore.OnSendScore -= HandleonSendScore;
    }

    void Start()
    {
        UpdateUI();
    }

    void HandleonUpdateHealth(int newHealth)
    {
        health = newHealth;
        UpdateUI();
    }

    void HandleonSendScore(int theScore)
    {
        score += theScore;
        UpdateUI();
    }

    void UpdateUI()
    {
        if (Application.loadedLevelName == "Zombie Shooter Level 1")
        {
            gameInfo = "Score: " + score.ToString() + "\nHealth: " + health.ToString();
        }
        else if (Application.loadedLevelName == "GameOver")
        {
            gameInfo = "Score: " + score.ToString();
        }
    }

    void OnGUI()
    {
        GUI.Box(boxRect, gameInfo);
    }
}
