﻿using UnityEngine;
using System.Collections;

public class ZombieBehaviour : MonoBehaviour
{

    public int health = 2;
    public int damage = 10;
    public GameObject explosionPrefab;
    public GameObject powerupPrefab;
    private int drop;
    public float adjustExplosionAngle = 0.0f;

    private Transform player;

    void Start()
    {
        if (GameObject.FindWithTag("Player"))
        {
            player = GameObject.FindWithTag("Player").transform;

            GetComponent<MoveTowardsObject>().target = player;
            GetComponent<SmoothLookAtTarget2D>().target = player;
        }
    }

    void OnCollisionEnter2D(Collision2D other){
        if(other.gameObject.CompareTag("Player")){
            other.gameObject.SendMessage("TakeDamage",damage);
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            Quaternion newRot = Quaternion.Euler(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + adjustExplosionAngle);

            Instantiate(explosionPrefab, transform.position, newRot);

            GetComponent<AddScore>().DoSendScore(); 

            if (GetComponent<AudioSource>() != null)
            {
                GetComponent<AudioSource>().Play();
            }

            drop = Random.Range(0, 11);
            if (drop == 10)
            {
                Instantiate(powerupPrefab, transform.position, transform.rotation);
            }

            Destroy(gameObject);
            
        }
    }

    void FixedUpdate()
    {
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        GetComponent<Rigidbody2D>().angularVelocity = 0.0f;
    }
}