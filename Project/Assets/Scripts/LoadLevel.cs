﻿using UnityEngine;
using System.Collections;

public class LoadLevel : MonoBehaviour {

    public GameObject Button;
    public string level;

    void Update()
    {
        if ((Input.GetMouseButtonDown(0)) && (gameObject == Button))
        {
            Application.LoadLevel(level);
        }
    }
}
