﻿using UnityEngine;
using System.Collections;

public class AltShootBullet : MonoBehaviour
{
    public GameObject bulletPrefab;
    public Transform bulletSpawn2;
    public float fireTime;
    public int powerUp = 0;

    private bool isFiring = false;

    public void PowerUp(bool activePowerUp)
    {
        if (activePowerUp == true)
        {
            powerUp = 1;        
        }

    }

    void SetFiring()
    {
        isFiring = false;
    }

    void Fire()
    {
        if ((powerUp == 1) && (fireTime >= 0.25))
            {
                fireTime = fireTime / 2;
                powerUp = 0;
            }

        isFiring = true;
        Instantiate(bulletPrefab, bulletSpawn2.position, bulletSpawn2.rotation);

        if (GetComponent<AudioSource>() != null)
        {
            GetComponent<AudioSource>().Play();
        }

        Invoke("SetFiring", fireTime);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButton(1))
        {
            if (!isFiring)
            {
                Fire();
            }
        }

    }
}