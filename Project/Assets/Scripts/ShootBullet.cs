﻿using UnityEngine;
using System.Collections;

public class ShootBullet : MonoBehaviour {
    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public float fireTime;
    public int powerUp = 0;

    private bool isFiring = false;

    

    public void PowerUp(bool activePowerUp)
    {
        if (activePowerUp == true)
        {
            powerUp = 1;
        }
        
    }

    void SetFiring()
    {
        isFiring = false;
    }

    void Fire()
    {
            if ((powerUp == 1) && (fireTime >= 0.25))
            {
                fireTime = fireTime/2;
                powerUp = 0;
            }

        isFiring = true;
        Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        if (GetComponent<AudioSource>() != null)
        {
            GetComponent<AudioSource>().Play();
        }

        Invoke("SetFiring", fireTime);
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButton(0))
        {
            if (!isFiring)
            {
                Fire();
            }
        }
	
	}
}
