﻿using UnityEngine;
using System.Collections;

public class PowerUpBehaviour : MonoBehaviour {

    public int health = 1;
    public bool activePowerUp;

	void Start () {

        activePowerUp = false;

	}

    public void TakeDamage(int damage)
    {
        health -= damage;

        if (health <= 0)
        {
            activePowerUp = true;
            Die();
        }
    }

    void Die()
    {
        Destroy(gameObject);
    }

}
