﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{

    public float destorytime = 0.7f;

    void Start()
    {
        Invoke("Die", destorytime);
    }

    void Die()
    {
        Destroy(gameObject);
    }
}